'''
Created on 23 mars 2014

@author: efarhan
'''
from animation.animation_main import Animation

class FoodAnimation(Animation):
    def __init__(self, obj):
        Animation.__init__(self, obj)
        self.eaten = False
        
    def update_animation(self, state="", invert=False):
        if self.eaten:
            self.state = 'eaten'
        else:
            self.state = 'not_eaten'
        return Animation.update_animation(self, state=state, invert=invert)
    def parse_animation(self,anim_data):
        pass