'''
Created on 22 mars 2014

@author: efarhan
'''
import math
from animation.animation_main import Animation
from json_export.json_main import get_element
from engine.vector import Vector2
from engine.const import log, CONST
from engine.physics import move, get_body_position, RayCastClosestCallback,\
    cast_ray
from engine.init import get_screen_size
from engine.font_manager import load_text, load_font
from engine.image_manager import show_image, draw_line
from engine.loop import get_screen
from engine.level_manager import get_level
from game_object.text import Text
from engine.sound_manager import load_sound, play_sound, switch_music,\
    get_music_index, music_index
from game_object.image import Image


class DogAnimation(Animation):
    def __init__(self, obj):
        Animation.__init__(self, obj)
        self.positions = []
        self.pos_index = 0
        self.stop_timer = 0
        self.direction = Vector2()
        self.rotate_diff = Vector2()
        self.target_pos = None
        self.exclamation = Image("data/sprites/sign/exclamation.png", (0,0))
        self.wouf_sound = load_sound("data/sound/dog_bark.ogg")
    def update_animation(self, state="", invert=False):
        self.state = 'move'
        center_pos = None
        if self.obj.not_rot_pos:
            center_pos = self.obj.not_rot_pos + self.obj.size/2
        elif self.obj.pos:
            center_pos = self.obj.pos + self.obj.size/2
        if len(self.positions) and self.obj.rect.collide_point(self.positions[self.pos_index]):
            self.stop_timer = 2*CONST.framerate
            self.pos_index = (self.pos_index+1)%len(self.positions)

        
        if self.target_pos:
            self.exclamation.pos = center_pos+self.direction*self.obj.size.y*3/4-self.exclamation.size/2
            self.exclamation.loop(get_screen(), get_level().screen_pos)
            if get_level().player.anim.chased > 0 and get_music_index() == 0:
                switch_music()
            self.direction = self.target_pos-center_pos
            self.direction.normalize()
        elif self.stop_timer != 0:
            self.rotate_diff = (self.positions[self.pos_index]-center_pos)
            self.rotate_diff.normalize()
            self.rotate_diff = self.rotate_diff - self.direction

            self.direction = self.direction + self.rotate_diff/float(self.stop_timer)
      
            self.direction.normalize()
            self.stop_timer -= 1
        else:
            self.direction = self.positions[self.pos_index]-center_pos
            self.direction.normalize()
        if not self.target_pos:
            if get_level().player.anim.chased == 0 and get_music_index() == 1:
                switch_music()
        '''Calculate angle and rotate'''
        v_0 = Vector2().coordinate(0, 1)
        dot_product = v_0.dot(self.direction)
        angle = math.acos(dot_product)*180/math.pi
        if self.direction.x > 0:
            angle = (-angle)%360
        self.obj.set_angle(angle)

       
        
        if self.stop_timer == 0:
            move(self.obj.body, vx=self.direction.x*self.speed, vy=self.direction.y*self.speed)
            
        else:
            move(self.obj.body,0,0)
        
        hit = False
        callback = RayCastClosestCallback()
        #center_pos = get_body_position(self.obj.body)
        origin = center_pos+self.direction*self.obj.size.y/2
        if CONST.debug:
            draw_line(get_screen(), origin-get_level().screen_pos, self.direction)
        cast_ray(callback, origin, origin+self.direction*get_screen_size().y/2)
        if callback.hit and callback.fraction < 1.0:
            if callback.fixture.userData == 2:
                play_sound(self.wouf_sound,unic=True)
                if not self.target_pos and not hit:
                    '''PLay WOUFWOUF'''
                    
                    if get_music_index() == 0:
                        switch_music()
                    get_level().player.anim.chased += 1
                hit = True

                player_pos = get_body_position(get_level().player.body)
                self.target_pos = player_pos

                self.stop_timer = 0
            else:
                if not hit:
                    self.target_lost(callback)
        else:
            if not hit:
                self.target_lost(callback)
        
        physics_pos = get_body_position(self.obj.body)
        
        if physics_pos:
            diff = self.obj.size/2
            diff.rotate(self.obj.angle)
            pos = physics_pos-diff
        else:
            pos = self.obj.pos
        if self.obj.screen_relative_pos:
            pos = pos-self.obj.screen_relative_pos*get_screen_size()
        self.obj.pos = pos
        
        Animation.update_animation(self, state, invert)
        
    def target_lost(self,callback):
        
        if self.target_pos:
            new_pos_index = 0
            for i in range(1,len(self.positions)):
                if (self.positions[i]-self.obj.pos).length < (self.positions[new_pos_index]-self.obj.pos).length:
                    new_pos_index = i
            self.pos_index = new_pos_index
            
            
            get_level().player.anim.chased -= 1
            if get_level().player.anim.chased == 0 and get_music_index() == 1:
                switch_music()
        self.target_pos = None

    def parse_animation(self,anim_data):
        positions = get_element(anim_data,"positions")
        if positions:
            for pos in positions:
                self.positions.append(Vector2().tuple2(pos))
        speed = get_element(anim_data, "speed")
        if not speed:
            speed = CONST.move_speed
        self.speed = speed