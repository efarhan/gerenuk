'''
Created on 22 mars 2014

@author: efarhan
'''
import random
from extensions.animations.player_animation import PlayerAnimation
from engine.vector import Vector2
import math
from engine.physics import move, get_body_position
from engine.init import get_screen_size
from event.event_main import get_button
from json_export.json_main import get_element
from engine.const import log, CONST
from game_object.image import Image, AnimImage
from event.physics_event import get_physics_event
from extensions.animations.food_animations import FoodAnimation
from engine.loop import get_screen
from engine.level_manager import get_level, switch_level
from extensions.animations.dog_animations import DogAnimation
from engine.sound_manager import load_sound, play_sound, get_music_index
from levels.death import DeathScene
from levels.credits import Credits


class NekoAnimation(PlayerAnimation):
    def __init__(self, player):
        PlayerAnimation.__init__(self, player)
        self.speed_boost = 1
        self.smooth = 1
        self.food = 8
        self.chased = 0
        self.direction = Vector2().coordinate(1, 0)
        self.chomp = Image('data/sprites/sign/chomp.png',(0,0))
        self.miam = load_sound('data/sound/eat_composite.ogg')
        self.action_click_time = -1
        path = "data/sound/gamepad/"
        self.gamepad_sounds = [load_sound(path+"LTTP_Rupee1.wav"),
                         load_sound(path+"LTTP_Secret.wav"),
                         load_sound(path+"smb_1-up.wav"),
                         load_sound(path+"smb_coin.wav")]
        self.contacts = []
    def update_state(self):
        self.state = ''
        RIGHT = get_button('player_right')
        LEFT = get_button('player_left')
        UP = get_button('player_up')
        DOWN = get_button('player_down')
        SPEED = get_button('player_speed')
        ACTION = get_button('player_action')
        
        if get_music_index() == 1:
            get_level().main_text.set_text('Run with R-CTRL')
        
        for c in self.contacts:
            target = c
            if target.anim.__class__ == FoodAnimation:
                
                if (ACTION and self.action_click_time == -1) or self.action_click_time > 0 :
                    if self.action_click_time == -1:
                        if target.anim.eaten:
                            continue
                        '''Play sound miam'''
                        self.food -= 1
                        play_sound(self.miam)
                        self.action_click_time = CONST.framerate*3
                    target.anim.eaten = True
                    self.chomp.pos = target.pos
                    self.chomp.loop(get_screen(), get_level().screen_pos)
                    move(self.player.body,0,0)
                    self.action_click_time -= 1
                    return
        if not ACTION:
            self.action_click_time = -1
        for event in get_physics_event():
            if event.a.userData == 2 or event.b.userData == 2:
                target = None
                if event.a.userData.__class__ == AnimImage:
                    target = event.a.userData
                    
                if event.b.userData.__class__ == AnimImage :
                    target = event.b.userData
                if target and target.anim.__class__ == FoodAnimation:
                    get_level().main_text.set_text("Eat with ENTER")
                    if event.begin and not target.anim.eaten:
                        self.contacts.append(target)
                    elif not event.begin:
                        try:
                            self.contacts.remove(target)
                        except ValueError:
                            pass
                elif target and  target.anim.__class__ == DogAnimation:
                    self.death_dog()
                if event.begin and (event.a.userData == 12 or event.b.userData == 12):
                    '''play_randomly gamepad sounds'''
                    play_sound(random.choice(self.gamepad_sounds), unic=True)
        if RIGHT-LEFT or DOWN-UP:
            self.state = 'move'
            self.direction = Vector2().coordinate(float(RIGHT-LEFT),float(DOWN-UP))
            self.direction.normalize()
        if SPEED:
            self.speed_boost = 2
        else:
            self.speed_boost = 1
        
        '''Calculate angle and rotate'''
        v_0 = Vector2().coordinate(0.0, 1.0)
        dot_product = v_0.dot(self.direction)
        angle = math.acos(dot_product)*180/math.pi
        if self.direction.x > 0:
            angle = (-angle)%360
        
        diff_angle = (angle-self.obj.angle)
        if (-180 > diff_angle):
            diff_angle = 360+diff_angle
        if (180 < diff_angle):
            diff_angle = 360-diff_angle
        self.obj.set_angle((self.obj.angle+(diff_angle)*self.smooth))

        
        if RIGHT-LEFT or DOWN-UP:
            move(self.obj.body, vx=self.direction.x*self.speed*self.speed_boost, vy=self.direction.y*self.speed*self.speed_boost)
        else:
            move(self.obj.body,0,0)
        
        physics_pos = get_body_position(self.player.body)
        
        if physics_pos:
            diff = self.player.size/2
            diff.rotate(self.player.angle)
            pos = physics_pos-diff
        else:
            pos = self.player.pos
        if self.player.screen_relative_pos:
            pos = pos-self.player.screen_relative_pos*get_screen_size()
        self.player.pos = pos
        if self.food == 0:
            switch_level(Credits())
    def death_dog(self):
        switch_level(DeathScene("dog"))
    def death_human(self):
        switch_level(DeathScene("human"))
    def get_screen_pos(self):
        if self.player.not_rot_pos:
            return self.player.not_rot_pos+self.player.size/2
        else:
            return self.player.pos+self.player.size/2
    def parse_animation(self, anim_data):
        speed = get_element(anim_data,"speed")
        if speed:
            self.speed = speed
        PlayerAnimation.parse_animation(self, anim_data)