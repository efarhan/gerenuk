'''
Created on 23 mars 2014

@author: efarhan
'''
import math
from animation.animation_main import Animation
from engine.vector import Vector2
from json_export.json_main import get_element
from engine.physics import get_body_position, move
from engine.init import get_screen_size
from event.physics_event import physics_events
from engine.level_manager import get_level
from engine.sound_manager import load_sound, play_sound, switch_music
from engine.const import CONST
from game_object.image import Image, AnimImage
from engine.loop import get_screen
from levels.gamestate import GameState

class HumanAnimation(Animation):
    def __init__(self, obj):
        Animation.__init__(self, obj)
        self.alert_time = False
        self.timer = 0
        self.direction = Vector2().coordinate(1, 0)
        self.smooth =1
        self.speed = 3
        self.sign = AnimImage()
        self.sign.pos = Vector2()
        self.sign.anim = Animation(self.sign)
        self.sign.anim.path = "data/sprites/"
        self.sign.anim.path_list = ["sign/"]
        self.sign.anim.state_range = {"interrogation":[2,3],"exclamation":[1,2]}
        self.sign.anim.load_images()
        self.detected_sound = load_sound("data/sound/detected1.ogg")
    def update_event(self,event):
        if event.a.userData == self.obj or event.b.userData == self.obj:
            fixture = None
            target_fixture = None
            if event.a.userData == self.obj:
                fixture = event.a
                target_fixture = event.b
            else:
                fixture = event.b
                target_fixture = event.a
            if target_fixture.userData == 2 and fixture.sensor:
                if event.begin:
                    '''set alert
                    set timer
                    play_detect sound'''
                    play_sound(self.detected_sound)
                    self.timer = 2*CONST.framerate
                    self.alert_time = True
                    get_level().player.anim.chased+=1
                    switch_music()
                else:
                    self.timer = 0
                    self.alert_time = False
                    get_level().player.anim.chased-=1
                    switch_music()
            if target_fixture.userData == 2 and not fixture.sensor:
                get_level().player.anim.death_human()
    def update_animation(self, state="", invert=False):
        
        if not self.alert_time:
            move(self.obj.body,0,0)
            self.state = ''
        for event in physics_events:
            self.update_event(event)
        if get_level().__class__ != GameState:
            return
        if self.alert_time:
            
            self.direction = get_body_position(get_level().player.body)-get_body_position(self.obj.body)
            self.direction.normalize()
            self.sign.pos = get_body_position(self.obj.body)-Vector2().coordinate(0, self.obj.size.y)
            if self.timer == 0:
                self.state = 'move'
                '''show exclamation'''
                self.sign.anim.state = "exclamation"
                self.sign.loop(get_screen(), get_level().screen_pos)
                move(self.obj.body,self.direction.x*self.speed,self.direction.y*self.speed)
            else:
                '''show interrogation'''
                self.sign.anim.state = "interrogation"
                self.sign.loop(get_screen(), get_level().screen_pos)
                move(self.obj.body,0,0)
                self.timer -= 1
        '''not contact_sensor and timer
        alert false'''
        
        '''if alert and timer < 0
        target_pos raycast
        if hit then player chase +1'''
        
        '''if miaou not target_pos'''
        
        '''if contact: cat death'''
        '''Calculate angle and rotate'''
        v_0 = Vector2().coordinate(0.0, 1.0)
        dot_product = v_0.dot(self.direction)
        angle = math.acos(dot_product)*180/math.pi
        if self.direction.x > 0:
            angle = (-angle)%360
        
        diff_angle = (angle-self.obj.angle)
        if (-180 > diff_angle):
            diff_angle = 360+diff_angle
        if (180 < diff_angle):
            diff_angle = 360-diff_angle
        self.obj.set_angle((self.obj.angle+(diff_angle)*self.smooth))
        
        physics_pos = get_body_position(self.obj.body)
        
        if physics_pos:
            diff = self.obj.size/2
            diff.rotate(self.obj.angle)
            pos = physics_pos-diff
        else:
            pos = self.obj.pos
        if self.obj.screen_relative_pos:
            pos = pos-self.obj.screen_relative_pos*get_screen_size()
        self.obj.pos = pos
        return Animation.update_animation(self, state=state, invert=invert)
    def parse_animation(self,anim_data):
        direction = get_element(anim_data,"direction")
        if direction:
            self.direction = Vector2().tuple2(direction)