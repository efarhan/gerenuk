
import sys
from engine.const import log, CONST
from engine.vector import Vector2
from json_export.json_main import load_json
from event.event_main import add_button

if CONST.render == 'pygame':
	import pygame
elif CONST.render == 'sfml':
	import sfml

def init_all():
	if CONST.render == 'pygame':
		pygame.init()
	screen = init_screen()
	if CONST.render == 'sfml':
		log(str(screen.settings))
	init_joystick()
	init_actions()
	return screen
def init_actions():
	actions = load_json(CONST.actions)
	for key in actions.items():
		log(key)
		add_button(key[0],key[1])
def init_screen():
	screen_size = CONST.screen_size
	if CONST.render == 'pygame':
		pygame.mouse.set_visible(False)
		pygame.font.init()
		
		return pygame.display.set_mode(screen_size,pygame.RESIZABLE)
	elif CONST.render == 'sfml':
		desktop = sfml.VideoMode.get_desktop_mode()
		style = sfml.Style.DEFAULT
		if CONST.fullscreen:
			style = sfml.Style.FULLSCREEN
		window = sfml.RenderWindow(desktop,'Kudu Window',style)
		return window
def init_joystick():
	pass
def get_screen_size():

	return Vector2().coordinate(CONST.screen_size[0],CONST.screen_size[1])
def toogle_fullscreen():
	from engine.loop import get_screen,set_screen
	screen = get_screen()
	size = screen.get_size()
	flags = 0
	if screen.get_flags() == pygame.RESIZABLE:
		flags = pygame.FULLSCREEN | pygame.RESIZABLE
		screen_info = pygame.display.Info()
		size = (screen_info.current_w, screen_info.current_h)
	set_screen(pygame.display.set_mode(size,flags))
def resize_screen(w,h):
	from engine.loop import get_screen,set_screen
	screen = get_screen()
	size = (w,h)
	flags = screen.get_flags()
	set_screen(pygame.display.set_mode(size,flags))