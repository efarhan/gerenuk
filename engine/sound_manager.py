'''
Manage sound and music 
'''

from engine.const import log, CONST
if CONST.render == 'pygame':
	import pygame
elif CONST.render == 'sfml':
	import sfml

sounds = {}
permanent_sound = []
playlist = []
music_index = 0
musics = []
sounds_playing = []

def get_music_index():
	global music_index
	return music_index

def set_playlist(music_list):
	'''
	Set a new playlist and play the first element
	'''
	global playlist,musics
	playlist = music_list
	if CONST.render == 'pygame':
		pygame.mixer.music.load(playlist[0])
		pygame.mixer.music.play()
	elif CONST.render == 'sfml':
		if len(musics):
			musics[music_index].stop()
		musics = []
		for music_filename in playlist:
			musics.append(sfml.Sound(sfml.SoundBuffer.from_file(music_filename)))
		if len(musics):
			musics[0].play()
		
def add_music_to_playlist(self,name):
	'''
	Add a music at the end of the playlist
	'''
	global playlist
	playlist.append(name)
def switch_music():
	'''Only works if two musics are on the playlist'''
	global playlist,music_index
	if CONST.render == 'pygame':
		if len(playlist) == 2:
			set_music_index((music_index+1)%len(playlist), pygame.mixer.music.get_pos())
	elif CONST.render == 'sfml':
		if len(playlist) == 2:
			set_music_index((music_index+1)%len(playlist), musics[music_index].playing_offset)
def set_music_index(index,pos):
	global playlist,music_index,last_pos,musics
	if index < len(playlist):
		if music_index != index:
			musics[music_index].stop()
			music_index = index
			if CONST.render == 'pygame':
				pygame.mixer.music.load(playlist[music_index])
				last_pos = float(pos)
				pygame.mixer.music.play(0,last_pos/1000)
			elif CONST.render == 'sfml':

				musics[music_index].play()
				musics[music_index].playing_offset = pos
				
def fadeout_music(t=0):
	'''Fadeout and then stop it after time t (seconds)'''
	if(pygame.mixer.music.get_busy()):
		if(t != 0):
			pygame.mixer.music.fadeout(t)
		else:
			pygame.mixer.music.fadeout(1)

def play_music(name):
	'''
	Set the playlist as one element and play it
	'''
	global playlist
	set_playlist([name])
	
def update_music_status():
	'''
	Switch to next music if it's over,
	must be called to have smooth transition
	'''
	global musics,music_index,playlist,sounds_playing
	if CONST.render == 'pygame':
		if(not pygame.mixer.music.get_busy()):
			music_index += 1
			music_index = music_index%len(playlist)
			pygame.mixer.music.load(playlist[music_index])
			pygame.mixer.music.play()
	elif CONST.render == 'sfml':
		if len(musics):
			if musics[music_index].status == sfml.Sound.STOPPED:
				music_index += 1
				music_index = music_index%len(musics)
				musics[music_index].play()
			
		delete_sounds = []
		for s in sounds_playing:
			if s.status == sfml.Sound.STOPPED:
				'''TODO: remove sound'''
				delete_sounds.append(s)
		for s in delete_sounds:
			sounds_playing.remove(s)
		del delete_sounds
def check_music_status():
	'''
	Return True if a music is currently playing
	'''
	if CONST.render == 'pygame':
		return pygame.mixer.music.get_busy()
	elif CONST.render == 'sfml':
		return musics[music_index].status == sfml.Sound.STOPPED

def load_sound(name,permanent=False):
	'''Load a sound in the system and returns it'''
	global sounds,permanent_sound
	try:
		sounds[name]
	except KeyError:
		if CONST.render == 'pygame':
			sounds[name] = pygame.mixer.Sound(name)
		elif CONST.render == 'sfml':
			sounds[name] = sfml.SoundBuffer.from_file(name)
	if permanent:
		permanent_sound.append(name)
	return sounds[name]

def play_sound(sound,unic=False):
	'''
	Plays a given sound
	'''
	global sounds_playing
	if CONST.render == 'pygame':
		sound.play()
	elif CONST.render == 'sfml':
		if unic:
			for s in sounds_playing:
				if s.buffer == sound:
					return
		sound_playing = sfml.Sound(sound)
		sound_playing.play()
		sounds_playing.append(sound_playing)


