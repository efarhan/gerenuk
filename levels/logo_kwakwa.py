from levels.scene import Scene
from engine.init import get_screen_size
from engine.const import CONST
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, load_sound,\
	play_sound
from levels.gamestate import GameState
from engine.vector import Vector2
from levels.title import TitleScreen
#font_obj, msg, sound_obj

class Kwakwa(Scene):
	def init(self):
		self.text = Image('data/sprites/text/kwakwa.png', 
						(get_screen_size()/2).get_tuple())
		self.text.pos = self.text.pos-self.text.size/2
		self.count = 4*CONST.framerate
		self.sound = load_sound("data/sound/pissed_off_duck.wav")
		self.first = True
	def loop(self, screen):
		fill_surface(screen,255, 255, 255)
		if self.first:
			play_sound(self.sound)
			self.first = False
		
		self.text.loop(screen, Vector2())
		if(not self.count):
			from engine.level_manager import switch_level
			switch_level(TitleScreen())
		else:
			self.count -= 1

			
