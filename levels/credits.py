'''

Created on 8 mars 2014

@author: efarhan
'''
from levels.scene import Scene
from engine.init import get_screen_size
from engine.image_manager import fill_surface
from game_object.image import Image
from engine.sound_manager import play_music, check_music_status, set_playlist
from levels.gamestate import GameState
from engine.font_manager import load_font
from game_object.text import Text
from engine.vector import Vector2
#font_obj, msg, sound_obj

class Credits(Scene):
    def init(self):
        self.font = 'data/font/acmesa.TTF'
        self.size = 100
        self.image = Image('data/sprites/text/end_credits.png',(0,0))
        self.texts = [
            "Meow: the Great Starvation",
            "a game by Team KwaKwa",
            " ",
            "Lead Designer",
            "Elias Farhan",
            " ",
            "Artists",
            "Dan Iorgulescu",
            "Val Andre-Terramorsi",
            " ",
            "Music & SFX",
            "Hamza 'Tenchi' Haiken",
            " ",
            " ",
            "Thanks for playing"
                    
        ]
        for i in range(len(self.texts)):
            self.texts[i] = Text(Vector2(), self.size, self.font, self.texts[i], color=(255,255,255))
            self.texts[i].pos = Vector2().coordinate(get_screen_size().x/2-self.texts[i].size.x/2,self.texts[i].pos.y+i*self.size)
        set_playlist(['data/music/loop_detected.ogg'])
        self.count = -get_screen_size().y
        
    def loop(self, screen):
        fill_surface(screen,0,0,0)
        self.image.loop(screen, Vector2())
        for i in range(len(self.texts)):
            self.texts[i].loop(screen,Vector2().coordinate(0,self.count))
        self.count += 1
        if(self.count >= self.size*len(self.texts)):
            from engine.level_manager import switch_level
            switch_level(0)


            
