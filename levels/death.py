'''
Created on 23 mars 2014

@author: efarhan
'''
from levels.scene import Scene
from game_object.image import AnimImage
from animation.animation_main import Animation
from engine.vector import Vector2
from engine.sound_manager import set_playlist, load_sound, play_sound,\
    update_music_status

class DeathScene(Scene):
    def __init__(self,death_type):
        Scene.__init__(self)
        self.death_type = death_type
    def init(self):
        set_playlist([])
        self.death_image = AnimImage()
        self.death_image.pos = Vector2()
        self.death_image.anim = Animation(self.death_image)
        self.death_image.anim.path = "data/sprites/"
        self.death_image.anim.path_list = ["death/","dog_death/"]
        self.death_image.anim.state_range = {"human": [0,50],"dog":[50,50+78]}
        self.death_image.anim.state = self.death_type
        self.death_image.anim.loop = False
        self.death_image.anim.load_images()
        self.death_image.anim.anim_freq = 2
        self.unhappy_cat = load_sound('data/sound/unhappy.ogg')
        self.kick_sound = load_sound('data/sound/kick.ogg')
        self.dog_sound = load_sound('data/sound/dog_bark.ogg')
    def loop(self, screen):
        update_music_status()
        if self.death_image.anim.state == 'human':
            if self.death_image.anim.find_index == 16:
                play_sound(self.kick_sound,unic=True)
                play_sound(self.unhappy_cat,unic=True)
        elif self.death_image.anim.state == 'dog':
            if self.death_image.anim.find_index == 1:
                play_sound(self.unhappy_cat,unic=True)
            if 35+4*7 > self.death_image.anim.find_index > 35 and (self.death_image.anim.find_index-35)%7 == 0:
                play_sound(self.dog_sound, unic=True)
        self.death_image.loop(screen, Vector2())
    def exit(self, screen):
        return
        return Scene.exit(self, screen)