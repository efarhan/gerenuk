'''
Created on 23 mars 2014

@author: efarhan
'''
from levels.scene import Scene
from game_object.image import Image
from engine.image_manager import fill_surface
from engine.vector import Vector2
from engine.sound_manager import load_sound, play_music, check_music_status
from engine.level_manager import switch_level
from levels.gamestate import GameState
from engine.const import CONST

class TitleScreen(Scene):
    def init(self):
        Scene.init(self)
        self.image = Image("data/sprites/text/title.png", (0,0))
        self.intro = play_music("data/sound/intro.ogg")
    def loop(self, screen):
        fill_surface(screen, 0,0,0,255)
        self.image.loop(screen, Vector2())
        Scene.loop(self, screen)
        if check_music_status():
            switch_level(GameState(CONST.startup))